/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-use-before-define */
import React, {useState} from 'react';
import {injectIntl} from 'react-intl';

import {
    UncontrolledDropdown,
    DropdownItem,
    DropdownToggle,
    DropdownMenu,
    Input,
} from 'reactstrap';

import {NavLink} from 'react-router-dom';
import {connect} from 'react-redux';

import IntlMessages from '../../../utils/helpers/IntlMessages';
import {
    setContainerClassnames,
    clickOnMobileMenu,
    logoutUser,
    changeLocale,
} from '../../../redux/types/general_types';

import {
    menuHiddenBreakpoint,
    searchPath,
    localeOptions,
    isDarkSwitchActive,
    buyUrl,
    adminRoot,
} from '../../../utils/constants/defaultValues';

import {MobileMenuIcon, MenuIcon} from '../../svg';
import TopnavEasyAccess from './Topnav.EasyAccess';
import TopnavNotifications from './Topnav.Notifications';
import TopnavDarkSwitch from './Topnav.DarkSwitch';

import {getDirection, setDirection} from '../../../utils/helpers/Utils';

const TopNav = ({
                    intl,
                    history,
                    containerClassnames,
                    menuClickCount,
                    selectedMenuHasSubItems,
                    setContainerClassnamesAction,
                    clickOnMobileMenuAction,
                    logoutUserAction,
                }) => {
    const [searchKeyword, setSearchKeyword] = useState('');

    const handleLogout = () => {
        logoutUserAction(history);
    };

    const menuButtonClick = (e, _clickCount, _conClassnames) => {
        e.preventDefault();

        setTimeout(() => {
            const event = document.createEvent('HTMLEvents');
            event.initEvent('resize', false, false);
            window.dispatchEvent(event);
        }, 350);
        setContainerClassnamesAction(
            _clickCount + 1,
            _conClassnames,
            selectedMenuHasSubItems
        );
    };

    const mobileMenuButtonClick = (e, _containerClassnames) => {
        e.preventDefault();
        clickOnMobileMenuAction(_containerClassnames);
    };

    const {messages} = intl;
    return (
        <nav className="navbar fixed-top">
            <div className="d-flex align-items-center navbar-left">
                <NavLink
                    to="#"
                    location={{}}
                    className="menu-button d-none d-md-block"
                    onClick={(e) =>
                        menuButtonClick(e, menuClickCount, containerClassnames)
                    }
                >
                    <MenuIcon/>
                </NavLink>
                <NavLink
                    to="#"
                    location={{}}
                    className="menu-button-mobile d-xs-block d-sm-block d-md-none"
                    onClick={(e) => mobileMenuButtonClick(e, containerClassnames)}
                >
                    <MobileMenuIcon/>
                </NavLink>

            </div>
            <NavLink className="navbar-logo" to={adminRoot}>
                <span className="logo d-none d-xs-block"/>
                <span className="logo-mobile d-block d-xs-none"/>
            </NavLink>

            <div className="navbar-right">

                <div className="user d-inline-block">
                    <UncontrolledDropdown className="dropdown-menu-right">
                        <DropdownToggle className="p-0" color="empty">
                            <span className="name mr-1">Milagritos Angeles</span>
                            <span>
                <img alt="Profile" src="/assets/img/profiles/l-1.jpg"/>
              </span>
                        </DropdownToggle>
                        <DropdownMenu className="mt-3" right>
                            <DropdownItem>Account</DropdownItem>
                            <DropdownItem>Features</DropdownItem>
                            <DropdownItem>History</DropdownItem>
                            <DropdownItem>Support</DropdownItem>
                            <DropdownItem divider/>
                            <DropdownItem onClick={() => handleLogout()}>
                                Sign out
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </div>
            </div>
        </nav>
    );
};

const mapStateToProps = ({menu, settings}) => {
    const {containerClassnames, menuClickCount, selectedMenuHasSubItems} = menu;
    const {locale} = settings;
    return {
        containerClassnames,
        menuClickCount,
        selectedMenuHasSubItems,
        locale,
    };
};
export default injectIntl(
    connect(mapStateToProps, {
        setContainerClassnamesAction: setContainerClassnames,
        clickOnMobileMenuAction: clickOnMobileMenu,
        logoutUserAction: logoutUser,
        changeLocaleAction: changeLocale,
    })(TopNav)
);
