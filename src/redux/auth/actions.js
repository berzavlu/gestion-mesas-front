// eslint-disable-next-line import/no-cycle
import axios from 'axios';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGOUT_USER,
    LOGIN_USER_ERROR,
} from '../types/general_types';
import {setCurrentUser} from "../../utils/helpers/Utils";

const API_URL = process.env.REACT_APP_URL_API
const API_LOGIN = '/login'

export const loginUser = (user) => async (dispatch) => {
    dispatch({type: LOGIN_USER});
    try {
        const response = await axios.post(API_URL + API_LOGIN, user)
        setCurrentUser(response.data);
        dispatch({
            type: LOGIN_USER_SUCCESS,
            payload: response.data,
            isAuth:true,
        })
    } catch (error) {
        dispatch({
            type: LOGIN_USER_ERROR,
            payload: 'Credenciales incorrectas'
        })
    }

    /*axios.post(API_URL + API_LOGIN, {user}).then(function (response) {
        console.log(response.data);
        dispatch({
            type: LOGIN_USER_SUCCESS,
            payload:response.data
        })
    }).catch(function (error) {
        dispatch({
            type: LOGIN_USER_ERROR,
            payload: 'Credenciales incorrectas'
        })
    });*/
    /*type: LOGIN_USER,
    payload: {user, history},*/
};
export const loginUserSuccess = (user) => ({
    type: LOGIN_USER_SUCCESS,
    payload: user,
});
export const loginUserError = (message) => ({
    type: LOGIN_USER_ERROR,
    payload: {message},
});
export const logoutUser = (history) => ({
    type: LOGOUT_USER,
    payload: {history},
});
