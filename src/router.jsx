import React, {Suspense} from 'react';
import {
    Route,
    Switch,
    BrowserRouter, Redirect,
} from 'react-router-dom';
import {IntlProvider} from 'react-intl';
import AppLocale from "./utils/lang";
import {NotificationContainer} from "./components/common/react-notifications";
import {adminRoot, isMultiColorActive, UserRole} from "./utils/constants/defaultValues";
import ColorSwitcher from "./components/common/ColorSwitcher";
import {ProtectedRoute} from "./utils/helpers/authHelper";


const ViewApp = React.lazy(() =>
    import(/* webpackChunkName: "views-app" */ '../src/pages/app')
);
const ViewUser = React.lazy(() =>
    import(/* webpackChunkName: "views-user" */ '../src/pages/user')
);
const ViewError = React.lazy(() =>
    import(/* webpackChunkName: "views-error" */ '../src/pages/error')
);
const ViewUnauthorized = React.lazy(() =>
    import(/* webpackChunkName: "views-error" */ '../src/pages/unauthorized')
);

const ViewLogin = React.lazy(() =>
    import(/* webpackChunkName: "views-user" */ '../src/pages/user/login')
);

function MainRouter() {
    const currentAppLocale = AppLocale['es'];
    return (
        <div className="h-100">
            <IntlProvider
                locale={currentAppLocale.locale}
                messages={currentAppLocale.messages}>
                <>
                    <NotificationContainer/>
                    <Suspense fallback={<div className="loading"/>}>
                        <BrowserRouter>
                            <Switch>
                                <ProtectedRoute
                                    path={adminRoot}
                                    component={ViewApp}
                                    roles={[UserRole.Admin, UserRole.Editor]}
                                />
                                <Route
                                    path="/dashboard"
                                    render={(props) => <div>Vista de página App</div>}
                                />
                                <Route
                                    path="/user"
                                    render={(props) => <ViewUser {...props} />}
                                />
                                <Route
                                    path="/error"
                                    exact
                                    render={(props) => <ViewError {...props} />}
                                />
                                <Route
                                    path="/unauthorized"
                                    exact
                                    render={(props) => <ViewUnauthorized {...props} />}
                                />
                                <Route
                                    path="/"
                                    exact
                                    render={(props) => <ViewLogin {...props} />}
                                />
                                {/*
                  <Redirect exact from="/" to={adminRoot} />
                  */}
                                <Redirect to="/error"/>
                            </Switch>
                        </BrowserRouter>
                    </Suspense>
                </>
            </IntlProvider>
        </div>
    );
}


export default MainRouter;