import { adminRoot } from './defaultValues';

const data = [
  /*{
    id: 'usuarios',
    icon: 'iconsminds-air-balloon-1',
    label: 'Usuarios',
    to: `${adminRoot}/gogo`,
    subs: [
      {
        icon: 'simple-icon-paper-plane',
        label: 'menu.start',
        to: `${adminRoot}/gogo/start`,
      },
    ],
  },*/
  {
    id: 'usuarios',
    icon: 'iconsminds-three-arrow-fork',
    label: 'Usuarios',
    to: `${adminRoot}/second-menu`,
    // roles: [UserRole.Admin, UserRole.Editor],
  },
  {
    id: 'categoria',
    icon: 'iconsminds-bucket',
    label: 'Categoria',
    to: `${adminRoot}/blank-page`,
  },
  {
    id: 'producto',
    icon: 'iconsminds-bucket',
    label: 'Producto',
    to: `${adminRoot}/blank-page`,
  },

];
export default data;
